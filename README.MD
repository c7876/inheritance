# Code reuse

## 1. Implementation inheritance (aka class inheritance)
Is done using the extends keyword in class declaration.

Final and extends keyword: classes marked as final cannot be extended 
<br> Example can be found: com.example.inheritance.finalClasses
<br>
Implicit inheritance: Each class which is defined will extend by default from Object class.  
### a. Inheriting fields
    Child classes will inherit the fields of Parent Class and have the ability to add additional fields.
    Access modifier details:
        public, protected fields are inherited
        default(without mentioning an access modifier) - inherited if the classes are on the same package
        private - are not inherited

    Example can be found in: package: com.example.inheritance.fields

### b. Inheriting methods 
    Child classes will inherit the methods of Parent Class and have the ability to add additional methods.
    Access modifier rules are the same as for fields.

    Example can be found in: package: com.example.inheritance.methods

## 2. Hiding fields and static methods
### a. Hiding fields
    Redeclare the same field inherited from the superclass in a subclass.
    Fields are in both classes, they might have different values.
    From subclass the hidden field can be access by prefixing with super keyword.
    In case the the hidden field is public, it can be access from outside by referencing the parent class.

    Example can be found in:com.example.inheritance.hiding.fields
### b. Hiding static methods
    Static methods cannot be overriden they can be hidden by redeclaring the same method in a child class.
    Static methods are ussualy access using the class name. 
    Based on the class name the correct static method is called.
    In case a static method is access via an instance variable(not a good practice),
    depending on the type of the instance variable the static method from that type will be called.

    Example can be found in: com.example.inheritance.hiding.staticmethods
### c. Override instance methods
    Instance methods cannot be hiding can only be overridden.
    reference via child class variable will call the children overriden method
    reference via parent class variable will call the children overriden method

    Example can be found in: com.example.inheritance.hiding.methods
## 3. Type inheritance (aka interface inheritance)
    In Java we have interfaces which prior to Java 8 could be considered pure abstract classes(an abstract class with all the methods abstract without any concreate implementation).
    Even if Java doesn't allow multiple inheritance via extend keyword. 
    Java allows implementing multiple interfaces via the implements keyword followed by the interfaces.
    The implements keyword must be after the extends keyword if exists.
    To sum up Java allows to extend one class and implement multiple interfaces.
    
    Example can be found in: com.example.inheritance.multipletypes
## 4. Default methods and multiple implementation inheritance
    In Java 8 default methods were introduce, which provide a default implementation to a method. 
    Default methods were introduce to give the possibility to add new methods in an interface without forcing 
    the client of the interface to implement the new methods.

    As a class can implement multiple interfaces so multiple default methods are inherited.
    If these 2 interfaces have 2 default method with the same signature,
    in the concrete implementation class it will force you to override the default method.

    Even if we can inherit multiple default methods we cannot inherit fields from multiple interfaces as 
    interfaces doesn't allow instance level fields. All the fields of an interface are public, static and, final by default.
    
    Example can be found in: com.example.inheritance.defaultmethods
    
    Diamon problem: Intial problem in c++: https://www.makeuseof.com/what-is-diamond-problem-in-cpp/
    So the problem was that the parent class fields were inherited from multiple classes and it can be solved by using virtual in C++.
    In java we cannot inherit instance level fields from multiple classes/interfaces so this particular flavor of the issue cannot happen.
    In java(after java 8) we can have a setup to create a 'diamond' structure. 

    Example can be found in: com.example.inheritance.diamond
   
