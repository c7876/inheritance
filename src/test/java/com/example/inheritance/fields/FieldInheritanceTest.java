package com.example.inheritance.fields;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class FieldInheritanceTest {

    @Test
    void testAdditionalFields() {
        final Set<String> someClassFields = Arrays.stream(Some.class.getFields()).map(field -> field.getName()).collect(Collectors.toSet());
        final Set<String> someWithAdditionalFields = Arrays.stream(SomeWithAdditionalFields.class.getFields()).map(field -> field.getName()).collect(Collectors.toSet());
        final Set<String> someWithAdditionalInheritance = Arrays.stream(SomeWithFieldInheritance.class.getFields()).map(field -> field.getName()).collect(Collectors.toSet());
        System.out.println(someClassFields);
        System.out.println(someWithAdditionalFields);
        System.out.println(someWithAdditionalInheritance);
    }
}
