package com.example.inheritance.multipletypes;

class MultipleTypesClient {

    public static void main(String[] args) {
        TClass typesClient = new TClass();
        typesClient.method1();
        typesClient.method2();

        Interface1 sameObjButWithCastedInterface1 = typesClient;
        sameObjButWithCastedInterface1.method1();
        // sameObjButWithCastedInterface1.method2();

        Interface2 sameObjButWithCastedInterface2 = typesClient;
        //sameObjButWithCastedInterface2.method1();
        sameObjButWithCastedInterface2.method2();
    }
}
