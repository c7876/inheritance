package com.example.inheritance.hiding.methods;

class OverridingMethodsChildClass extends BaseClass {

    @Override
    public String getField() {
        return "overriden method which add this prefix text:" + super.getField();
    }
}
