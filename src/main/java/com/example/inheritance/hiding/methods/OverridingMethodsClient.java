package com.example.inheritance.hiding.methods;

class OverridingMethodsClient {

    public static void main(String[] args) {
        BaseClass baseObj = new BaseClass();
        baseObj.setField("Field");

        System.out.println("Base class getter:" + baseObj.getField());

        RegularChildClass regularChildObj = new RegularChildClass();
        // calling inherited setter field
        regularChildObj.setField("Regular Field");
        System.out.println("Calling inherited getter from base:" + regularChildObj.getField());

        OverridingMethodsChildClass overridingMethodsChildObj = new OverridingMethodsChildClass();
        overridingMethodsChildObj.setField("Value");
        System.out.println("Calling overriden method:" + overridingMethodsChildObj.getField());
        BaseClass overridingMethodsChildObjViaBaseClass = overridingMethodsChildObj;
        System.out.println("Calling overriden method:" + overridingMethodsChildObjViaBaseClass.getField());

    }
}
