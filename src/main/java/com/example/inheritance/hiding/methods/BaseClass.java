package com.example.inheritance.hiding.methods;

class BaseClass {
    private String field;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
