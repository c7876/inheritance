package com.example.inheritance.hiding.fields;

class HidingFieldsClient {

    public static void main(String[] args) {
        BaseClass baseObject = new BaseClass();
        System.out.println(baseObject);
        baseObject.field = "Base Field";
        System.out.println(baseObject);
        System.out.println();

        RegularChildClass regularChildClass = new RegularChildClass();
        regularChildClass.field = "Child field inherited from base class";
        System.out.println(regularChildClass);
        System.out.println();

        ChildClassHidingField childObjectWithHidingField = new ChildClassHidingField();
        childObjectWithHidingField.field = "Child Field from the child class which hides the base class field";
        System.out.println(childObjectWithHidingField);
        // accessing hidden field from BaseClass by casting
        System.out.println(((BaseClass)childObjectWithHidingField).field);
        ((BaseClass)childObjectWithHidingField).field = "modified";
        System.out.println(childObjectWithHidingField);
        childObjectWithHidingField.modifyBaseField();
        System.out.println(childObjectWithHidingField);
        System.out.println();

        BaseClass x = new ChildClassHidingField();
        System.out.println(x.field);
        ChildClassHidingField y = (ChildClassHidingField)x;
        System.out.println(y.field);

    }
}
