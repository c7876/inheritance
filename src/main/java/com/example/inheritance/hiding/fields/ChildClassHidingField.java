package com.example.inheritance.hiding.fields;

class ChildClassHidingField extends BaseClass {
    public String field;

    @Override
    public String toString() {
        return "ChildClassHidingField{" +
                "fieldFromThisClass='" + field + '\'' +
                ", fieldFromBaseClass='" + super.field + '\'' +
                '}';
    }

    public void modifyBaseField() {
        super.field = "Access base class field and modify it!";
    }
}
