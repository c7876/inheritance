package com.example.inheritance.hiding.fields;

class BaseClass {

    public String field;

    public BaseClass() {
        this.field="Default Base field value";
    }

    @Override
    public String toString() {
        return "BaseClass{" +
                "field='" + field + '\'' +
                '}';
    }
}
