package com.example.inheritance.hiding.fields;

class RegularChildClass extends BaseClass {

    @Override
    public String toString() {
        return "RegularChildClass{" +
                "fieldFromBaseClass='" + field + '\'' +
                '}';
    }
}
