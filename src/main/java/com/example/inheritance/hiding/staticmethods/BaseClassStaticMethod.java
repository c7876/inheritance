package com.example.inheritance.hiding.staticmethods;

class BaseClassStaticMethod {

    public static void staticMethod() {
        System.out.println("Base class static method!");
    }

    public static void extraStaticMethod() {
        System.out.println("Base class extra static method!");
    }
}
