package com.example.inheritance.hiding.staticmethods;

class ClientStaticMethod {

    public static void main(String[] args) {
        BaseClassStaticMethod.staticMethod();
        // static method hiding
        ChildClassStaticMethod.staticMethod();

        BaseClassStaticMethod.extraStaticMethod();
        // accessing inherited static method
        ChildClassStaticMethod.extraStaticMethod();

        BaseClassStaticMethod baseInstance = new BaseClassStaticMethod();
        baseInstance.staticMethod();
        ChildClassStaticMethod childInstance = new ChildClassStaticMethod();
        childInstance.staticMethod();
        // casting to base class will call the static method from base class
        ((BaseClassStaticMethod)childInstance).staticMethod();

    }
}
