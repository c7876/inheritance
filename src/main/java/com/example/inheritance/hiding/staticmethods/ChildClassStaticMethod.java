package com.example.inheritance.hiding.staticmethods;

class ChildClassStaticMethod extends BaseClassStaticMethod {

    public static void staticMethod() {
        System.out.println("Child class static method!");
    }
}
