package com.example.inheritance.methods;

class AClassWithMethodInheritance extends AClass {

    // avoid redefining method 'void method()' from class AClass by using extends syntax

    public void methodOtherMethod() {
        System.out.println("AClass method!");
    }
}
