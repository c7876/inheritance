package com.example.inheritance.methods;

class AClassWithAdditionalMethods {

    // is a copy from the class AClass
    public void method() {
        System.out.println("AClass method!");
    }

    public void methodOtherMethod() {
        System.out.println("AClass method!");
    }
}
