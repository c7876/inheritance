package com.example.inheritance.diamond.afterJava8;

interface BaseInterface {

//    void method();
    default void method() {
        System.out.println("Base interface");
    }
}
