package com.example.inheritance.diamond.afterJava8;

class DiamondJava8 implements ChildInterface1, ChildInterface2 {
//    @Override
//    public void method() {
//        System.out.println("Overridden method");
//    }
    @Override
    public void method() {
        ChildInterface1.super.method();
    }
}
