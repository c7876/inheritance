package com.example.inheritance.diamond.afterJava8;

interface ChildInterface1 extends BaseInterface {

    @Override
    default void method() {
        BaseInterface.super.method();
        System.out.println("Child interface 1");
    }

//    void method();
}
