package com.example.inheritance.diamond.afterJava8;

class DiamondJava8Client {
    public static void main(String[] args) {
        DiamondJava8 diamondJava8 = new DiamondJava8();

        BaseInterface baseInterface = diamondJava8;

        baseInterface.method();
    }
}
