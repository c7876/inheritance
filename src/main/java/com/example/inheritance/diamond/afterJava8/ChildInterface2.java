package com.example.inheritance.diamond.afterJava8;

interface ChildInterface2 extends BaseInterface {

    @Override
    default void method() {
        System.out.println("Child interface 2");
    }
//
//    void method();
}
