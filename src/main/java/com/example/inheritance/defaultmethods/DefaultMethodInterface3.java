package com.example.inheritance.defaultmethods;

interface DefaultMethodInterface3 {

    default void method1() {
        System.out.println("default method 1 defined in Interface 3");
    }

}
