package com.example.inheritance.defaultmethods;

interface DefaultMethodInterface1 {

    default void method1() {
        System.out.println("default method 1");
    }

}
