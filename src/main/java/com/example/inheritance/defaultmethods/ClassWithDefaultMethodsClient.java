package com.example.inheritance.defaultmethods;

class ClassWithDefaultMethodsClient {

    public static void main(String[] args) {
        DClass dClass = new DClass();

        dClass.method1();
        dClass.method2();


        DefaultMethodInterface1 sameObjButWithCastedInterface1 = dClass;
        sameObjButWithCastedInterface1.method1();
        // sameObjButWithCastedInterface1.method2();

        DefaultMethodInterface2 sameObjButWithCastedInterface2 = dClass;
        //sameObjButWithCastedInterface2.method1();
        sameObjButWithCastedInterface2.method2();

        DClass otherReference = (DClass) sameObjButWithCastedInterface1;
        otherReference.method2();
    }
}
