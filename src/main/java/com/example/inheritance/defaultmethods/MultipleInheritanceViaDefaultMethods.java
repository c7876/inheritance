package com.example.inheritance.defaultmethods;

class MultipleInheritanceViaDefaultMethods implements DefaultMethodInterface1, DefaultMethodInterface3{
    @Override
    public void method1() {
        DefaultMethodInterface1.super.method1();
    }
}
