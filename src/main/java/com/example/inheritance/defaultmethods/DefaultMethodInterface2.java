package com.example.inheritance.defaultmethods;

interface DefaultMethodInterface2 {

    default void method2() {
        System.out.println("default method 2");
    }
}
