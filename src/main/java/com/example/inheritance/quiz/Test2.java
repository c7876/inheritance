package com.example.inheritance.quiz;


class Category {
    Category() {
        System.out.print("Category_");
    }
}
class SubCategory extends Category {
    SubCategory() {
        System.out.print("SubCategory_");
    }
}
class SubSubCategory extends SubCategory {
    SubSubCategory() {
        System.out.print("SubSubCategory_");
    }
}
public class Test2 {
    public static void main(String[] args) {
        new SubSubCategory();
    }
}

