package com.example.inheritance.fields;

class SomeWithFieldInheritance extends Some {
    // avoid redefining 'String a' from class Some by using extends syntax
    public String b;
    public String c;
}
