package com.example.inheritance.fields;

class SomeWithAdditionalFields {
    public String a; // is a copy from the class Some
    public String b;
    public String c;
}
